Overlord Setup
=========

Ansible role to setup my workflow on my main desktop

Requirements
------------

```shell
ansible [core 2.16.4]
```

Role Variables
--------------

```shell
TODO
```

Dependencies
------------

None.

Example Playbook
----------------

```shell
---
- name: Setup overlord PC
  hosts: localhost
  
  roles:
  - overlord-setup
```

License
-------

MIT

Author Information
------------------

Raif Coonjah (c) 2024
